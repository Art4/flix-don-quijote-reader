Flix Don Quijote Reader
=======================
Dies ist ein kleiner Reader, um sich die Strips zu [Don Quijote von Flix](http://www.nichtlustig-shop.de/product_info.php?info=p433_Don-Quijote.html) bequemer ansehen zu können.

Dazu einfach die index.html abspeichern und im Browser öffnen.

Dieser Reader [wurde ursprünglich](https://twitter.com/weigandtLabs/status/204947763824308227) auf [Pastebin.com](http://pastebin.com/7gU8N1Mi) gepostet.


